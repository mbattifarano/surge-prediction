import re
import subprocess as sh
import datetime
import time
from collections import OrderedDict
from contextlib import contextmanager
import numpy as np
import pandas as pd
from toolz import curry

connection = "postgresql://matt:postgres@localhost/cmu_mac"

UBER = 'uberx'
LYFT = 'lyft'
SERVICES = [UBER, LYFT]

data_hdf = 'data/surge-prediction-features.h5'
surge_key = 'surges_%s'
surge_vec_key = 'surge_state_%s'
cluster_distances_key = 'cluster_distances_%s'
features_key = 'features'
historical_avg_key = 'historical_average_%s'
historical_state_key = 'historical_surge_state_%s'

results_hdf = 'data/surge-prediction-results.%s.%s.h5'
prediction_key = 'predictions_%s'
performance_key = 'performance_%s'
feature_selection_key = 'feature_selection_%s'

cluster_centers_file = 'data/cluster-centers.%s.pkl'

tracts_shp = 'data/tracts'
places_shp = 'data/places'


@contextmanager
def simple_timer(name=None):
    print '[timer] starting timed task %s' % name or ''
    t0 = time.time()
    yield
    print "[timer] completed %s in %0.2fs" % (name or 'execution', time.time() - t0)


def to_dow(dt):
    return dt.weekday()


def to_minutes(dt):
    return (60 * dt.hour) + dt.minute


def mspe_score(y_true, y_pred):
    return 100 * np.mean(((y_pred - y_true) / y_true) ** 2)


def mspe_scorer(estimator, X, y):
    y_pred = estimator.predict(X)
    return - mspe_score(y, y_pred)


@curry
def relative_mspe_scorer(y_pred_naive, y_pred_hist, estimator, X, y):
    """Returns the difference between model and naive larger is better"""
    mspe_naive = mspe_score(y, y_pred_naive)
    mspe_hist = mspe_score(y, y_pred_hist)
    mspe_to_beat = min(mspe_naive, mspe_hist)
    mspe_estimator = mspe_score(y, estimator.predict(X))
    return mspe_to_beat - mspe_estimator


class TimeOfDay:
    def __init__(self):
        raise NotImplementedError()

    early_am = "Early_Morning"
    am_peak = "AM_Peak"
    mid_day = "Mid_Day"
    pm_peak = "PM_Peak"
    night = "Evening"
    late = "Late_Night"

    all_times = OrderedDict([
        (early_am, (3, 6)),
        (am_peak, (6, 9)),
        (mid_day, (9, 16)),
        (pm_peak, (16, 18)),
        (night, (18, 21)),
        (late, (21, 3)),
    ])

    @staticmethod
    def _between(h, bounds):
        s, e = bounds
        if s == e:
            return s == h
        elif s < e:
            return s <= h < e
        else:
            return (s <= h) | (h < e)

    @classmethod
    def classify(cls, dt):
        h = dt.hour
        for name, bounds in cls.all_times.iteritems():
            if cls._between(h, bounds):
                return name

    @classmethod
    def window_elapsed(cls, dt):
        window_name = cls.classify(dt)
        h_s, h_e = cls.all_times[window_name]
        date = dt.date()
        start_t = datetime.time(h_s)
        end_t = datetime.time(h_e)
        if h_e < h_s:
            if dt.hour <= h_e:
                end = datetime.datetime.combine(date, end_t)
                yesterday = date - datetime.timedelta(days=1)
                start = datetime.datetime.combine(yesterday, start_t)
            else:
                start = datetime.datetime.combine(date, start_t)
                tomorrow = date + datetime.timedelta(days=1)
                end = datetime.datetime.combine(tomorrow, end_t)
        else:
            start = datetime.datetime.combine(date, start_t)
            end = datetime.datetime.combine(date, end_t)
        total = (end - start).total_seconds()
        elapsed = (dt - start).total_seconds()
        return elapsed / total


def feature_type(f):
    if ':' in f:
        ftype, _ = f.split(':')
    else:
        ftype = 'other'
    return ftype


def round_up_to_nearest(n, x):
    x = int(x)
    rem = int(bool(x % n))
    return (rem + x/n)*n


def rediscretize_surge(sm):
    if sm <= 1.1:
        return 1
    elif sm <= 2.0:
        return round_up_to_nearest(25, 100*sm)/100.0
    else:
        return round_up_to_nearest(5, 10*sm)/10.0


def load_data(service):
    print "Loading %s data" % service

    df_surge_vectors = (pd.read_hdf(data_hdf, surge_vec_key % service).dropna())
    df_surge_vectors = df_surge_vectors.rename(columns={
        col: "surge:{}".format(col) for col in df_surge_vectors.columns
    })
    df_historical_surges = pd.read_hdf(data_hdf, historical_state_key % service)
    df_cluster_distances = pd.read_hdf(data_hdf, cluster_distances_key % service).reset_index()

    df_features = pd.read_hdf(data_hdf, features_key)

    cluster_id_dummies = pd.get_dummies(df_cluster_distances.cluster_id)
    df = df_surge_vectors.join(df_features, how='left').sort_index()

    freq_m = 10  # resample to every 10 minutes
    df = df.resample('%ds' % freq_m * 60).mean()
    print("%d of %d rows contain null values" % (len(df) - len(df.dropna()), len(df)))
    df = df.fillna(df.median())

    lookback_m = 60
    assert lookback_m % freq_m == 0, "sample frequency MUST divide lookback window duration"
    lookback_samples = lookback_m / freq_m

    # Create dataframe where each row contains data from the last lookback_m minutes
    index = df.index.values[lookback_samples:]
    columns = []
    data = []
    for i in xrange(lookback_samples):
        suffix = "@t%02d" % (freq_m * i)
        columns.extend([colname + suffix for colname in df.columns])  # label each feature with offset
        if i > 0:
            extract = df.values[(lookback_samples - i): -i, :]
        else:
            extract = df.values[(lookback_samples - i):, :]
        data.append(extract)
    data_cat = np.hstack(data)
    df_lookback = pd.DataFrame(data_cat, index=index, columns=columns)
    df_lookback.index.name = df.index.name
    return df_lookback, df_surge_vectors, df_historical_surges, df_cluster_distances, cluster_id_dummies


def git_sha():
    return sh.check_output('git rev-parse --short HEAD'.split()).strip()
