import re
import datetime
import geopandas as gpd
import pandas as pd
from support import data_hdf, features_key, surge_key, tracts_shp, places_shp, connection, SERVICES
import datetime_truncate as date_trunc


def tract_geodata():
    query = """
    SELECT tract_name, geom
    FROM surge_prediction.study_tracts;
    """
    df_tracts = gpd.GeoDataFrame.from_postgis(query, con=connection, geom_col='geom')
    df_tracts.to_file(tracts_shp)
    return df_tracts


def is_tract_col(colname):
    return colname.startswith('tract')


def parse_tract_col(colname):
    m = re.match('(.*):tract_([\d\.]*)', colname)
    assert m is not None, "No match found"
    feature, tract_name = m.groups()
    return tract_name, feature


def place_geodata():
    query = """
    SELECT name, geom
    FROM surge_prediction.places;
    """
    df_places = (gpd.GeoDataFrame.from_postgis(query, connection, geom_col='geom'))
    df_places.to_file(places_shp)
    return df_places


def main():
    tract_geodata()
    place_geodata()


if __name__ == '__main__':
    main()
