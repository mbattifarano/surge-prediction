import pandas as pd
import geopandas as gpd
import numpy as np
from toolz import curry
from support import (UBER, LYFT, connection, data_hdf, tracts_shp,
                     surge_key, surge_vec_key, historical_state_key, historical_avg_key, features_key)

compression = 9


def tract_index_map(tablename):
    query = """
    SELECT DISTINCT tract_name
    FROM surge_prediction.%s
    ORDER BY 1;
    """ % tablename
    df = pd.read_sql(query, connection)
    df['tract_idx'] = df.index.values
    return df


def surges(service):
    surge_query = """
    SELECT *
    FROM surge_prediction.estimated_surges_%s
    WHERE recorded_at BETWEEN '2016-10-01' AND '2017-04-01'
    """ % service
    df_surges = pd.read_sql(surge_query, connection).dropna().set_index(['recorded_at', 'place_label'])
    df_surges.to_hdf(data_hdf, surge_key % service, complevel=compression)
    return df_surges


def _to_place_vectors(df, surge_column):
    place_dummies = pd.get_dummies(df.place_label)
    place_values = df[[surge_column]].values * place_dummies.values
    df_vec = (pd.DataFrame(place_values,
                           index=place_dummies.index,
                           columns=place_dummies.columns)
                .groupby('recorded_at')
                .sum()
              )
    df_vec[df_vec < 1.0] = 1.0  # Some timepoints are missing data for some places -- essentially fillna
    return df_vec


def to_surge_vectors(df_surges, service):
    df_surges_ts = df_surges.reset_index(level=1).sort_index()
    df_surge_vectors = _to_place_vectors(df_surges_ts, 'surge_multiplier')
    df_surge_vectors.to_hdf(data_hdf, surge_vec_key % service, complevel=compression)
    return df_surge_vectors


def expanding_average_surge(service):
    query = """
    SELECT *
    FROM (
           SELECT
             esu.*,
             avg(esu.surge_multiplier)
             OVER w AS historical_avg,
             avg(esu.surge_multiplier_10)
             OVER w AS historical_avg_10,
             avg(esu.surge_multiplier_20)
             OVER w AS historical_avg_20,
             avg(esu.surge_multiplier_30)
             OVER w AS historical_avg_30,
             avg(esu.surge_multiplier_40)
             OVER w AS historical_avg_40,
             avg(esu.surge_multiplier_60)
             OVER w AS historical_avg_60,
             avg(esu.surge_multiplier_120)
             OVER w AS historical_avg_120
           FROM surge_prediction.estimated_surges_%s esu
           WHERE recorded_at < '2017-04-01'
           WINDOW w AS (
             PARTITION BY (place_label, recorded_at :: TIME)
             ORDER BY recorded_at
             ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING )
         ) a
    WHERE a.recorded_at >= '2016-10-01';
    """ % service
    df_hist_avg = (pd.read_sql(query, connection)
                     .dropna()
                     .set_index(['recorded_at', 'place_label'])
                     .sort_index()
                   )
    df_hist_avg.to_hdf(data_hdf, historical_avg_key % service, complevel=9)
    return df_hist_avg


def to_historical_vectors(df_hist_avg, service):
    df_historical = df_hist_avg.reset_index()[['recorded_at', 'place_label', 'historical_avg']].set_index('recorded_at')
    df_hist_vec = _to_place_vectors(df_historical, 'historical_avg')
    df_hist_vec.to_hdf(data_hdf, historical_state_key % service, complevel=compression)
    return df_hist_vec


@curry
def to_array(dtype, l):
    return np.array(l, dtype=dtype)


def unpack_vectors(col, dtype):
    return np.vstack(col.apply(to_array(dtype)).values)


def to_dataframe(data, index, label, tract_names):
    _, ncols = data.shape
    assert ncols == len(tract_names)
    return pd.DataFrame(
        data,
        columns=['%s:tract_%s' % (label, name) for name in tract_names],
        index=index
    )


def to_df_columns(df, colname, dtype, tract_names):
    data = unpack_vectors(df[colname], dtype)
    return to_dataframe(data, df.index, colname, tract_names)


def features():
    features_query = """
        SELECT
          w.*,
          stv.mean_speed as speed,
          stv.var_speed,
          etv.n_events as events,
          ctv.n_closures as closures
        FROM surge_prediction.interpolated_weather w
          JOIN surge_prediction.speed_tract_vectors stv ON w.recorded_at = stv.recorded_at
          JOIN surge_prediction.event_tract_vectors etv ON w.recorded_at = etv.recorded_at
          JOIN surge_prediction.closure_tract_vectors ctv ON w.recorded_at = ctv.recorded_at;
        """
    df_features = pd.read_sql(features_query, connection).dropna().set_index('recorded_at')

    tract_ids_speeds = tract_index_map('tract_congestion')
    tract_ids_events = tract_index_map('tract_events')
    tract_ids_rcrs = tract_index_map('tract_rcrs')

    df_speeds = to_df_columns(df_features, 'speed', np.float, tract_ids_speeds.tract_name)
    df_events = to_df_columns(df_features, 'events', np.uint8, tract_ids_events.tract_name)
    df_closures = to_df_columns(df_features, 'closures', np.uint8, tract_ids_rcrs.tract_name)

    drop_cols = ['speed', 'var_speed', 'events', 'closures']
    df_weather = (df_features.drop(drop_cols, axis=1)
                             .join(pd.get_dummies(df_features.condition, prefix='condition', drop_first=True))
                             .drop('condition', axis=1))
    df_weather = df_weather.rename(columns={
        col: "weather:{}".format(col) for col in df_weather.columns
    })
    df_features = (df_weather.join(df_speeds)
                             .join(df_events)
                             .join(df_closures)
                   )
    df_features.to_hdf(data_hdf, features_key, complevel=compression)
    return df_features


if __name__ == '__main__':
    print "Collecting Uber surge data"
    df_surges_uber = surges(UBER)
    df_surge_vectors_uber = to_surge_vectors(df_surges_uber, UBER)
    df_hist_surges_uber = expanding_average_surge(UBER)
    df_hist_vectors_uber = to_historical_vectors(df_hist_surges_uber, UBER)

    print "Collecting Lyft surge data"
    df_surges_lyft = surges(LYFT)
    df_surge_vectors_lyft = to_surge_vectors(df_surges_lyft, LYFT)
    df_hist_surges_lyft = expanding_average_surge(LYFT)
    df_hist_vectors_lyft = to_historical_vectors(df_hist_surges_lyft, LYFT)

    print "Collecting features"
    df_features = features()
    print "done."
