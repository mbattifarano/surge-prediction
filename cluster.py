import itertools
import pickle
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from support import TimeOfDay, SERVICES, data_hdf, cluster_centers_file, cluster_distances_key, surge_vec_key

dfs = {s: pd.read_hdf(data_hdf, surge_vec_key % s)
       for s in SERVICES
       }


def cluster_surges(df_surge_vectors, service):
    print "Clustering %s data" % service
    freq_m = 10
    df_surge_ts = df_surge_vectors.resample('%ds' % freq_m * 60).mean()
    print "%d of %d rows contain null values" % (len(df_surge_ts) - len(df_surge_ts.dropna()), len(df_surge_ts))
    df_surge_ts = df_surge_ts.fillna(df_surge_ts.median())

    tod_idx = df_surge_ts.reset_index().recorded_at.apply(TimeOfDay.classify).values
    times_of_day = [TimeOfDay.early_am, TimeOfDay.am_peak, TimeOfDay.mid_day, TimeOfDay.pm_peak, TimeOfDay.night,
                    TimeOfDay.late]
    n_samples = {
        TimeOfDay.night: 18,
        TimeOfDay.early_am: 18,
        TimeOfDay.am_peak: 18,
        TimeOfDay.late: 36,
        TimeOfDay.pm_peak: 12,
        TimeOfDay.mid_day: 42,
    }

    lookback = 6
    n_clusters = 4
    all_place_cluster_centroids = []
    all_place_upper_bounds = []
    all_place_lower_bounds = []

    def create_cluster_input(t):
        this_tod = (tod_idx == t)
        n = n_samples[t]
        if t == TimeOfDay.late:  # late is special because it straddles 2 days
            def key_func(i):
                return this_tod[i]

            indices_i = []
            for k, g in itertools.groupby(xrange(len(this_tod)), key_func):
                idx_group = list(g)
                if k and (len(idx_group) == n):
                    indices_i.extend(idx_group)
            indices = np.zeros_like(this_tod, dtype=np.bool)
            indices[indices_i] = True
        else:
            indices = this_tod
        X = (df_surge_ts.loc[indices].values  # n timepoints * n windows x n locations
                        .T  # n locations x n timepoints * n windows
                        .reshape([-1, n])  # n locations * n windows x  n timepoints
             )
        return X, indices

    cluster_distances = pd.DataFrame()
    feature_labels = pd.DataFrame()
    for t in times_of_day:
        X, original_indices = create_cluster_input(t)  # samples x time points
        cluster_df_index = df_surge_ts.loc[original_indices].index
        model = KMeans(n_clusters)
        model.fit(X)
        nT = n_samples[t]
        timepoint_place_labels = (np.tile(np.expand_dims(model.labels_, -1), (1, nT))
                                    .reshape(len(df_surge_ts.columns), -1)
                                    .T
                                  )
        tpt_labels = pd.DataFrame(timepoint_place_labels,
                                  index=cluster_df_index,
                                  columns=df_surge_ts.columns)
        feature_labels = feature_labels.append(tpt_labels)
        labels = pd.Series(model.labels_, name='labels')
        # order clusters by membership
        label_counts = (labels
            .groupby(labels)
            .count()
            .reindex(np.arange(n_clusters))
            .fillna(0)
            .sort_values(ascending=False)
            )
        cluster_order = label_counts.index.values
        centers = model.cluster_centers_[cluster_order, :]
        lower_bound = np.zeros_like(centers)
        upper_bound = np.zeros_like(centers)
        for i, ci in enumerate(cluster_order):
            upper_bound[i, :] = np.percentile(X[model.labels_ == ci, :], 90, axis=0)
            lower_bound[i, :] = np.percentile(X[model.labels_ == ci, :], 10, axis=0)
        assert model.cluster_centers_.shape == centers.shape
        all_place_cluster_centroids.append(centers)
        all_place_upper_bounds.append(upper_bound)
        all_place_lower_bounds.append(lower_bound)
        # compute distances between X and cluster centers (column-wise)
        window_distances = []
        n_locations = len(df_surge_ts.columns)
        for c in xrange(n_clusters):
            pointwise_distance = (X - centers[c, :]) ** 2
            mw_distances = []
            for i in xrange(X.shape[1]):
                window_start = max(i - lookback, 0)
                window_end = i + 1
                avg_dist_in_window = pointwise_distance[:, window_start:window_end].mean(axis=1)
                mw_distances.append(avg_dist_in_window)
            window_dist_by_loc = np.vstack(mw_distances).T.reshape(n_locations, -1).T
            window_distances.append(np.expand_dims(window_dist_by_loc, -1))
        window_distances = np.concatenate(window_distances, 2)
        for i in xrange(n_clusters):
            d = window_distances[:, :, i].squeeze()
            idx_col = cluster_df_index
            cluster_id_col = np.expand_dims(np.ones(d.shape[0]) * i, -1)
            data = np.hstack([cluster_id_col, d])
            df = pd.DataFrame(data, index=idx_col, columns=['cluster_id'] + list(df_surge_ts.columns))
            cluster_distances = cluster_distances.append(df)
    (cluster_distances.reset_index()
                      .set_index(['recorded_at', 'cluster_id'])
                      .to_hdf(data_hdf, cluster_distances_key % service, complevel=9)
     )
    cluster_describe = dict(
        n_clusters=n_clusters,
        tod_divs=[n_samples[tod] for tod in times_of_day],
        tod_names=times_of_day,
        centers=np.hstack(all_place_cluster_centroids),
        lowers=np.hstack(all_place_lower_bounds),
        uppers=np.hstack(all_place_upper_bounds),
    )
    with open(cluster_centers_file % service, 'wb') as fp:
        pickle.dump(cluster_describe, fp)
    return cluster_distances, cluster_describe


cluster_data = {}
for srv, df in dfs.iteritems():
    dists, desc = cluster_surges(df, srv)
    cluster_data[srv] = dict(
        distances=dists,
        description=desc,
     )
