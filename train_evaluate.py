import sys
import os
import itertools
import datetime
import numpy as np
import pandas as pd
from sklearn import model_selection, linear_model, pipeline, preprocessing, ensemble
from toolz import curry
from support import (TimeOfDay, to_dow, to_minutes, mspe_score, load_data, simple_timer,
                     prediction_key, performance_key, feature_selection_key, UBER, LYFT,
                     git_sha, results_hdf, mspe_scorer)
import datetime_truncate as date_trunc


DEBUG = 'debug'
FULL = 'full'
PARTIAL = 'partial'
RUN_STATE = None
DO_LASSO = False
DO_RF = False
COMMIT = git_sha()

if '--debug' in sys.argv:
    RUN_STATE = DEBUG
elif '--full' in sys.argv:
    RUN_STATE = FULL
else:
    RUN_STATE = PARTIAL

if '--lasso' in sys.argv:
    DO_LASSO = True

if '--random-forest' in sys.argv:
    DO_RF = True

print "pid: %d" % os.getpid()
print "version: %s" % COMMIT
print "mode: %s" % RUN_STATE

ALPHAS = np.arange(0.05, 0.21, 0.01)
MAX_LEAF_NODES = np.arange(2, 20, 2)
WARMUP_WEEKS = 20
TRAIN_WEEKS = 20
RESULTS_HDF = results_hdf % (RUN_STATE, COMMIT)


@curry
def future_target(horizon, dt):
    return pd.to_datetime(dt) + datetime.timedelta(minutes=horizon)


def time_series_split(X):
    week = X.index.map(date_trunc.truncate_week).values
    weeks = np.unique(week)

    for i, test_week in enumerate(weeks[WARMUP_WEEKS:]):
        discard_before = weeks[WARMUP_WEEKS + i - TRAIN_WEEKS]
        train_idx = (week < test_week) & (week >= discard_before)
        test_idx = (week == test_week)
        yield train_idx, test_idx


def train_evaluate(service):
    df_lookback, df_surge_vectors, df_historical_surges, df_cluster_distances, cluster_id_dummies = load_data(service)

    if RUN_STATE == DEBUG:
        horizons = [60]
        times_of_day = [TimeOfDay.night]
        target_places = ['Benedum Center']
        N_JOBS = 1
    elif RUN_STATE == PARTIAL:
        horizons = [10, 20, 30, 40, 60, 90, 120]
        times_of_day = TimeOfDay.all_times.keys()
        target_places = ['Benedum Center', 'South Side', 'Shadyside']
        N_JOBS = -1
    elif RUN_STATE == FULL:
        horizons = [30]
        times_of_day = TimeOfDay.all_times.keys()
        target_places = [c.split(':')[-1] for c in df_surge_vectors.columns]
        N_JOBS = -1
    else:
        raise RuntimeError("Unknown run state '%s'" % RUN_STATE)

    performance = pd.DataFrame()
    feature_selection = pd.DataFrame()
    _predictions_all = []

    recorded_at = df_lookback.index.values

    print "Training models on %s data" % service
    for place, horizon in itertools.product(target_places, horizons):
        with simple_timer('preparing features'):
            cluster_distance_columns = ['cluster:%02d' % i for i in xrange(4)]
            d_clusters = (pd.DataFrame(df_cluster_distances[[place]].values * cluster_id_dummies.values,
                                       index=df_cluster_distances.recorded_at,
                                       columns=cluster_distance_columns
                                       )
                            .groupby('recorded_at')
                            .sum()
                          )

            future = np.array(map(future_target(horizon), recorded_at))
            index_map = pd.Series(recorded_at, index=future, name='index_map')

            window_elapsed = pd.Series(map(TimeOfDay.window_elapsed, future),
                                       index=recorded_at,
                                       name='time:window_elapsed'
                                       )
            _hist = df_historical_surges.reindex(future)
            historical_mean = pd.DataFrame(_hist.fillna(_hist.mean()).values,
                                           index=recorded_at,
                                           columns=map('historical:{}'.format, _hist.columns),
                                           )
            dow = pd.Series(map(to_dow, future),
                            index=recorded_at,
                            name='time:day_of_week'
                            )
            minutes = pd.Series(map(to_minutes, future),
                                index=recorded_at,
                                name='time:minute_of_day'
                                )
            target = df_surge_vectors["surge:%s" % place].reindex(future).dropna()
            _prediction_timestamps = target.index.values

            select = index_map.reindex(_prediction_timestamps).values

            features_common = (df_lookback.reindex(select)
                                          .join(historical_mean)
                                          .join(d_clusters)
                               )
            for col in cluster_distance_columns:
                features_common[col] = features_common[col].fillna(features_common[col].mean())
            features_lasso = features_common.join(window_elapsed)
            features_rf = (features_common.join(dow)
                                          .join(minutes)
                           )
            tod_idx = target.index.map(TimeOfDay.classify).values
            assert len(features_common) == len(features_lasso) == len(features_rf)
            lasso_exclusive_features = set(features_lasso.columns) - set(features_common.columns)
            rf_exclusive_features = set(features_rf.columns) - set(features_common.columns)
            cv_outer = time_series_split(features_common)

            _predictions = []
            y_true = pd.Series(target, name='actual_surge_multiplier')

            _lasso_selected_features = set()
        for cv_iteration, (train_mask, test_mask) in enumerate(cv_outer):
            with simple_timer("%s @ t+%d fold %d" % (place, horizon, cv_iteration)):
                # Naive mean (baseline)
                y_test = target[test_mask]
                y_pred_test = features_common[train_mask]["surge:%s@t00" % place].mean()

                _performance_idx = pd.MultiIndex.from_tuples(
                    [(place, horizon, None, None, 'naive_mean')],
                    names=['place', 'horizon', 'time_of_day', 'cv_fold', 'model']
                )
                _performance_data = {
                    'test_mspe': mspe_score(y_test, y_pred_test),
                    'train_mspe': None,
                    'alpha': None,
                    'n_features_selected': None,
                    'n_samples': None,
                }
                print ("%s @ t+%d: naive mspe = %0.2f%%"
                       % (place, horizon, _performance_data['test_mspe']))
                performance = performance.append(pd.DataFrame(_performance_data,
                                                              index=_performance_idx))

                _prediction_index = (pd.MultiIndex.from_product([
                    [place], [horizon], [None], [None], ['naive_mean'], y_test.index.values
                ],
                    names=['place', 'horizon', 'time_of_day', 'cv_fold', 'model', 'recorded_at']
                )
                )
                _predictions.append(pd.Series(y_pred_test,
                                              index=_prediction_index,
                                              name='predicted_surge',
                                              )
                                    )

                # Historical average (baseline)
                y_test = target[test_mask]
                y_pred_test = features_common[test_mask]["historical:%s" % place].values

                _performance_idx = pd.MultiIndex.from_tuples(
                    [(place, horizon, None, None, 'historical_mean')],
                    names=['place', 'horizon', 'time_of_day', 'cv_fold', 'model']
                )
                _performance_data = {
                    'test_mspe': mspe_score(y_test, y_pred_test),
                    'train_mspe': None,
                    'alpha': None,
                    'n_features_selected': None,
                    'n_samples': None,
                }
                print ("%s @ t+%d: historical mean mspe = %0.2f%%"
                       % (place, horizon, _performance_data['test_mspe']))
                performance = performance.append(pd.DataFrame(_performance_data,
                                                              index=_performance_idx))

                _prediction_index = (pd.MultiIndex
                                       .from_product([
                                            [place], [horizon], [None], [None], ['historical_mean'], y_test.index.values
                                        ],
                                            names=['place', 'horizon', 'time_of_day', 'cv_fold', 'model', 'recorded_at']
                                        )
                                     )
                _predictions.append(pd.Series(y_pred_test,
                                              index=_prediction_index,
                                              name='predicted_surge',
                                              )
                                    )
                if DO_LASSO:
                    for tod in times_of_day:
                        # Lasso
                        cv_inner = model_selection.TimeSeriesSplit(3)
                        lasso = pipeline.Pipeline([
                            ('scaler', preprocessing.StandardScaler()),
                            ('lasso', linear_model.LassoCV(cv=cv_inner, normalize=False, alphas=ALPHAS, n_jobs=N_JOBS))
                        ])

                        tod_mask = (tod_idx == tod)
                        train_mask_lasso = train_mask & tod_mask

                        X_train = features_lasso[train_mask_lasso]
                        y_train = target[train_mask_lasso]
                        with simple_timer('lasso (%s)' % tod):
                            lasso.fit(X_train, y_train)
                        y_pred_train = lasso.predict(X_train)

                        test_mask_lasso = test_mask & tod_mask

                        X_test = features_lasso[test_mask_lasso]
                        y_test = target[test_mask_lasso]
                        y_pred_test = lasso.predict(X_test)

                        _lasso = lasso.named_steps['lasso']
                        non_zero_feature_names = set(X_train.columns[_lasso.coef_ != 0])
                        _lasso_selected_features.update(non_zero_feature_names)

                        _performance_idx = pd.MultiIndex.from_tuples(
                            [(place, horizon, tod, cv_iteration, 'lasso')],
                            names=['place', 'horizon', 'time_of_day', 'cv_fold', 'model']
                        )
                        _performance_data = {
                            'test_mspe': mspe_score(y_test, y_pred_test),
                            'train_mspe': mspe_score(y_train, y_pred_train),
                            'alpha': _lasso.alpha_,
                            'n_features_selected': len(non_zero_feature_names),
                            'n_samples': len(X_train),
                        }
                        performance = performance.append(pd.DataFrame(_performance_data, index=_performance_idx))

                        _named_coefs = dict(zip(features_lasso.columns, _lasso.coef_))
                        feature_selection = feature_selection.append(pd.DataFrame(_named_coefs, index=_performance_idx), sort=False)

                        _prediction_timestamps = y_test.index.values
                        _prediction_index = (pd.MultiIndex
                                               .from_product([
                                                    [place], [horizon], [tod], [cv_iteration], ['lasso'], _prediction_timestamps
                                                     ],
                                                    names=['place', 'horizon', 'time_of_day', 'cv_fold', 'model', 'recorded_at']
                                                )
                                             )
                        _predictions.append(pd.Series(y_pred_test,
                                                      index=_prediction_index,
                                                      name='predicted_surge',
                                                      )
                                            )

                        print ("%s @ t+%d (%s): lasso mspe = %0.2f%% with alpha = %0.2f (train mspe = %0.2f)" %
                               (place, horizon, tod, _performance_data['test_mspe'], _performance_data['alpha'], _performance_data['train_mspe']))

                if DO_RF:
                    # Random Forest with Lasso selected features
                    cv_inner = model_selection.TimeSeriesSplit(3)
                    rf_lasso_feature_names = np.array(list(
                        _lasso_selected_features.difference(lasso_exclusive_features)
                                                .union(rf_exclusive_features)
                    ))
                    features_rf_lasso = features_rf[list(rf_lasso_feature_names)]
                    random_forest_lasso_features = model_selection.GridSearchCV(
                        ensemble.RandomForestRegressor(n_estimators=10, n_jobs=N_JOBS),
                        {
                            'max_leaf_nodes': MAX_LEAF_NODES,
                         },
                        cv=cv_inner,
                        n_jobs=N_JOBS,
                        scoring=mspe_scorer,
                    )
                    X_train = features_rf_lasso[train_mask]
                    y_train = target[train_mask]
                    print len(X_train), len(y_train)
                    with simple_timer('random forest (lasso features)'):
                        random_forest_lasso_features.fit(X_train, y_train)
                    y_pred_train = random_forest_lasso_features.predict(X_train)

                    X_test = features_rf_lasso[test_mask]
                    y_test = target[test_mask]
                    y_pred_test = random_forest_lasso_features.predict(X_test)

                    rf = random_forest_lasso_features.best_estimator_
                    non_zero_feature_names = rf_lasso_feature_names[rf.feature_importances_ > 0.0]

                    _performance_idx = pd.MultiIndex.from_tuples(
                        [(place, horizon, None, cv_iteration, 'random_forest_lasso_features')],
                        names=['place', 'horizon', 'time_of_day', 'cv_fold', 'model']
                    )
                    _performance_data = {
                        'test_mspe': mspe_score(y_test, y_pred_test),
                        'train_mspe': mspe_score(y_train, y_pred_train),
                        'alpha': rf.max_leaf_nodes,
                        'n_features_selected': len(non_zero_feature_names),
                        'n_samples': len(X_train),
                    }
                    performance = performance.append(pd.DataFrame(_performance_data, index=_performance_idx))

                    _named_coefs = dict(zip(features_rf_lasso.columns, rf.feature_importances_))
                    feature_selection = feature_selection.append(pd.DataFrame(_named_coefs, index=_performance_idx), sort=False)

                    _prediction_timestamps = y_test.index.values
                    _prediction_index = (pd.MultiIndex
                                           .from_product([
                                                [place], [horizon], [None], [cv_iteration], ['random_forest_lasso_features'], _prediction_timestamps
                                            ],
                                                names=['place', 'horizon', 'time_of_day', 'cv_fold', 'model', 'recorded_at']
                                            )
                                         )
                    _predictions.append(pd.Series(y_pred_test,
                                                  index=_prediction_index,
                                                  name='predicted_surge',
                                                  )
                                        )
                    print ("%s @ t+%d: random forest with lasso features mspe = %0.2f%% with max_leaf_nodes = %d (train mspe = %0.2f)"
                           % (place, horizon, _performance_data['test_mspe'],  _performance_data['alpha'], _performance_data['train_mspe']))

                    # Random Forest with all features
                    cv_inner = model_selection.TimeSeriesSplit(3)
                    random_forest_all_features = model_selection.GridSearchCV(
                        ensemble.RandomForestRegressor(n_estimators=10, n_jobs=N_JOBS),
                        {
                            'max_leaf_nodes': MAX_LEAF_NODES,
                         },
                        cv=cv_inner,
                        n_jobs=N_JOBS,
                        scoring=mspe_scorer,
                    )
                    X_train = features_rf[train_mask]
                    y_train = target[train_mask]
                    with simple_timer('random forest (all features)'):
                        random_forest_all_features.fit(X_train, y_train)
                    y_pred_train = random_forest_all_features.predict(X_train)

                    X_test = features_rf[test_mask]
                    y_test = target[test_mask]
                    y_pred_test = random_forest_all_features.predict(X_test)

                    rf = random_forest_all_features.best_estimator_
                    non_zero_feature_names = features_rf.columns[rf.feature_importances_ > 0.0]
                    _performance_idx = pd.MultiIndex.from_tuples(
                        [(place, horizon, None, cv_iteration, 'random_forest_all_features')],
                        names=['place', 'horizon', 'time_of_day', 'cv_fold', 'model']
                    )
                    _performance_data = {
                        'test_mspe': mspe_score(y_test, y_pred_test),
                        'train_mspe': mspe_score(y_train, y_pred_train),
                        'alpha': rf.max_leaf_nodes,
                        'n_features_selected': len(non_zero_feature_names),
                        'n_samples': len(X_train),
                    }
                    performance = performance.append(pd.DataFrame(_performance_data, index=_performance_idx))
                    _named_coefs = dict(zip(features_rf.columns, rf.feature_importances_))
                    feature_selection = feature_selection.append(pd.DataFrame(_named_coefs, index=_performance_idx), sort=False)

                    _prediction_timestamps = y_test.index.values
                    _prediction_index = (pd.MultiIndex
                                           .from_product([
                                                [place], [horizon], [None], [cv_iteration], ['random_forest_all_features'], _prediction_timestamps
                                            ],
                                                names=['place', 'horizon', 'time_of_day', 'cv_fold', 'model', 'recorded_at']
                                            )
                                         )
                    _predictions.append(pd.Series(y_pred_test,
                                                  index=_prediction_index,
                                                  name='predicted_surge',
                                                  )
                                        )
                    print ("%s @ t+%d: random forest mspe = %0.2f%% with max_leaf_nodes = %d (train mspe = %0.2f)" %
                           (place, horizon, _performance_data['test_mspe'], _performance_data['alpha'], _performance_data['train_mspe']))
        _predictions_all.append(pd.DataFrame(pd.concat(_predictions)).join(y_true))

    predictions = pd.concat(_predictions_all)

    print "saving to hdf"
    predictions.to_hdf(RESULTS_HDF, prediction_key % service, complevel=9)
    performance.to_hdf(RESULTS_HDF, performance_key % service, complevel=9)
    feature_selection.to_hdf(RESULTS_HDF, feature_selection_key % service, complevel=9)


if __name__ == '__main__':
    if RUN_STATE in [DEBUG, PARTIAL, FULL]:
        train_evaluate(UBER)
    if RUN_STATE in [DEBUG, PARTIAL]:
        train_evaluate(LYFT)
    metadata = pd.DataFrame().append({
        'commit': COMMIT,
        'run_mode': RUN_STATE,
        'alphas': ALPHAS,
        'max_leaf_nodes': MAX_LEAF_NODES,
        'warmup_weeks': WARMUP_WEEKS,
        'train_weeks': TRAIN_WEEKS,
        'utcnow': datetime.datetime.utcnow(),
    }, ignore_index=True)
    metadata.to_hdf(RESULTS_HDF, 'metadata')
