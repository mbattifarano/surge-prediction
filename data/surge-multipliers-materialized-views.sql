SET search_path = "surge_prediction", "public";

CREATE MATERIALIZED VIEW IF NOT EXISTS surge_prediction.estimated_surges_uberx
  AS
    WITH surges AS (
        SELECT
          uber_cost.place_label,
          uber_cost.request_date AT TIME ZONE 'America/New_York' AS recorded_at,
          surge_prediction.round_timestamp(uber_cost.request_date,
                                           (SELECT bin_width_s :: INTEGER AS bin_width_s
                                            FROM surge_prediction.etl_parameters)) AT TIME ZONE
          'America/New_York'                                     AS recorded_at_bin,
          LEAST(uber_cost.surge_multiplier, 4.9)                 AS surge_multiplier
        FROM surge_prediction.uber_cost
        WHERE (uber_cost.display_name = 'uberX' :: TEXT)
    ), binned_surges AS (
        SELECT
          tb.place_label,
          tb.time_bin,
          S.recorded_at,
          S.surge_multiplier
        FROM (surge_prediction.place_time_raster tb
          LEFT JOIN surges S ON (((S.recorded_at_bin = tb.time_bin) AND (S.place_label = tb.place_label))))
    ), interpolated_surges AS (
        SELECT
          binned_surges.place_label,
          binned_surges.time_bin,
          binned_surges.recorded_at,
          binned_surges.surge_multiplier,
          avg(binned_surges.surge_multiplier)
          OVER (
            PARTITION BY binned_surges.place_label
            ORDER BY binned_surges.time_bin, binned_surges.recorded_at
            ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING ) AS interpolated_surge
        FROM binned_surges
    ), estimated_surges AS (
        SELECT
          interpolated_surges.place_label,
          interpolated_surges.time_bin AS recorded_at,
          max(COALESCE(interpolated_surges.surge_multiplier,
                       interpolated_surges.interpolated_surge,
                       1.0))           AS surge_multiplier
        FROM interpolated_surges
        GROUP BY interpolated_surges.place_label, interpolated_surges.time_bin
    )
    SELECT
      es.place_label,
      es.recorded_at,
      es.surge_multiplier,
      lead(es.surge_multiplier, 1)
      OVER future AS surge_multiplier_10,
      lead(es.surge_multiplier, 2)
      OVER future AS surge_multiplier_20,
      lead(es.surge_multiplier, 3)
      OVER future AS surge_multiplier_30,
      lead(es.surge_multiplier, 4)
      OVER future AS surge_multiplier_40,
      lead(es.surge_multiplier, 6)
      OVER future AS surge_multiplier_60,
      lead(es.surge_multiplier, 12)
      OVER future AS surge_multiplier_120
    FROM estimated_surges es
    WINDOW future AS (
      PARTITION BY es.place_label
      ORDER BY es.recorded_at
      RANGE BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING );

CREATE MATERIALIZED VIEW IF NOT EXISTS surge_prediction.estimated_surges_lyft
  AS
    WITH surges AS (
        SELECT
          place_label,
          request_date AT TIME ZONE 'America/New_York' AS recorded_at,
          surge_prediction.round_timestamp(request_date,
                                           (SELECT bin_width_s :: INTEGER AS bin_width_s
                                            FROM surge_prediction.etl_parameters)) AT TIME ZONE
          'America/New_York'                                     AS recorded_at_bin,
          surge_multiplier                 AS surge_multiplier
        FROM surge_prediction.lyft_cost
        WHERE (ride_type = 'lyft')
    ), binned_surges AS (
        SELECT
          tb.place_label,
          tb.time_bin,
          S.recorded_at,
          S.surge_multiplier
        FROM (surge_prediction.place_time_raster tb
          LEFT JOIN surges S ON (((S.recorded_at_bin = tb.time_bin) AND (S.place_label = tb.place_label))))
    ), interpolated_surges AS (
        SELECT
          binned_surges.place_label,
          binned_surges.time_bin,
          binned_surges.recorded_at,
          binned_surges.surge_multiplier,
          avg(binned_surges.surge_multiplier)
          OVER (
            PARTITION BY binned_surges.place_label
            ORDER BY binned_surges.time_bin, binned_surges.recorded_at
            ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING ) AS interpolated_surge
        FROM binned_surges
    ), estimated_surges AS (
        SELECT
          interpolated_surges.place_label,
          interpolated_surges.time_bin AS recorded_at,
          max(COALESCE(interpolated_surges.surge_multiplier,
                       interpolated_surges.interpolated_surge,
                       1.0))           AS surge_multiplier
        FROM interpolated_surges
        GROUP BY interpolated_surges.place_label, interpolated_surges.time_bin
    )
    SELECT
      es.place_label,
      es.recorded_at,
      es.surge_multiplier,
      lead(es.surge_multiplier, 1)
      OVER future AS surge_multiplier_10,
      lead(es.surge_multiplier, 2)
      OVER future AS surge_multiplier_20,
      lead(es.surge_multiplier, 3)
      OVER future AS surge_multiplier_30,
      lead(es.surge_multiplier, 4)
      OVER future AS surge_multiplier_40,
      lead(es.surge_multiplier, 6)
      OVER future AS surge_multiplier_60,
      lead(es.surge_multiplier, 12)
      OVER future AS surge_multiplier_120
    FROM estimated_surges es
    WINDOW future AS (
      PARTITION BY es.place_label
      ORDER BY es.recorded_at
      RANGE BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING );


CREATE MATERIALIZED VIEW IF NOT EXISTS surge_prediction.weather_features
  AS
    SELECT
      w.recorded_at,
      max(w.wind_speed)          AS wind_speed,
      avg(w.temperature)         AS temperature,
      max(w.precipitation)       AS precipitation,
      bool_or(w.is_preciptating) AS is_precipitating,
      first(condition)           AS condition
    FROM (SELECT
            round_timestamp(weather.recorded_at, 3600) AT TIME ZONE 'America/New_York' AS recorded_at,
            COALESCE(weather.wind_gust, weather.wind_speed)                            AS wind_speed,
            weather.temperature,
            (weather.rain OR weather.snow)                                             AS is_preciptating,
            COALESCE(weather.precipitation, 0)                                         AS precipitation,
            weather.condition
          FROM weather
          WHERE (weather.zipcode = '15222' :: TEXT)) w
    GROUP BY w.recorded_at;

CREATE MATERIALIZED VIEW IF NOT EXISTS surge_prediction.tract_events
  AS
    SELECT
      st.tract_name,
      st.recorded_at,
      count(e.id) AS n_events
    FROM surge_prediction.tract_time_raster st
      LEFT JOIN surge_prediction.events e
        ON e.geom && st.geom
           AND st.recorded_at BETWEEN e.start_date AND e.end_date
    GROUP BY tract_name, recorded_at;

CREATE MATERIALIZED VIEW IF NOT EXISTS surge_prediction.tract_rcrs
  AS
    SELECT
      st.tract_name,
      st.recorded_at,
      count(rcrs.eventid) AS n_closures
    FROM surge_prediction.tract_time_raster st
      LEFT JOIN (
                  SELECT
                    rcrs.eventid,
                    rcrs.date_time_closed AT TIME ZONE 'America/New_York'     AS closed_at,
                    rcrs.act_date_time_opened AT TIME ZONE 'America/New_York' AS opened_at,
                    rcrs.geom
                  FROM surge_prediction.rcrs
                  WHERE ((rcrs.status = 'OPEN' :: TEXT) AND (rcrs.geom IS NOT NULL) AND
                         (rcrs.date_time_closed IS NOT NULL) AND (rcrs.act_date_time_opened IS NOT NULL))
                ) rcrs
        ON rcrs.geom && st.geom
           AND st.recorded_at BETWEEN rcrs.closed_at AND rcrs.opened_at
    GROUP BY tract_name, recorded_at;

CREATE MATERIALIZED VIEW IF NOT EXISTS surge_prediction.speed_tmcs
  AS
    SELECT
      speeds.*,
      tmcs.geom
    FROM surge_prediction.speeds
      JOIN surge_prediction.tmcs ON speeds.tmc_code = tmcs.tmc_id;

CREATE INDEX IF NOT EXISTS speed_tmcs_geom_idx
  ON surge_prediction.speed_tmcs USING GIST (geom);

CREATE MATERIALIZED VIEW IF NOT EXISTS surge_prediction.tract_tmcs
  AS
    SELECT
      tmcs.tmc_id,
      tracts.tract_name
    FROM surge_prediction.tmcs tmcs
      JOIN surge_prediction.study_tracts tracts
        ON tracts.geom && tmcs.geom;

CREATE INDEX IF NOT EXISTS tract_tmcs_tmc_id_idx
  ON surge_prediction.tract_tmcs (tmc_id);

CREATE MATERIALIZED VIEW IF NOT EXISTS surge_prediction.tract_congestion
  AS
    SELECT
      st.tract_name,
      st.recorded_at,
      avg(spd.speed)      AS mean_speed,
      variance(spd.speed) AS var_speed,
      min(spd.speed)      AS min_speed,
      max(spd.speed)      AS max_speed
    FROM surge_prediction.tract_time_raster st
      JOIN (
             SELECT
               tt.tract_name,
               surge_prediction.round_timestamp(spd.measurement_tstamp, 600) AT TIME ZONE
               'America/New_York'                     AS recorded_at,
               coalesce(spd.speed, spd.average_speed) AS speed
             FROM surge_prediction.speeds spd
               JOIN surge_prediction.tract_tmcs tt ON tt.tmc_id = spd.tmc_code
           ) spd
        ON st.tract_name = spd.tract_name AND st.recorded_at = spd.recorded_at
    GROUP BY st.tract_name, st.recorded_at;

CREATE INDEX IF NOT EXISTS weather_features_recorded_at_idx
  ON surge_prediction.weather_features (recorded_at);

CREATE UNIQUE INDEX IF NOT EXISTS tract_rcrs_recorded_at_tract_name_idx
  ON surge_prediction.tract_rcrs (recorded_at, tract_name);

CREATE UNIQUE INDEX IF NOT EXISTS tract_events_recorded_at_tract_name_idx
  ON surge_prediction.tract_events (recorded_at, tract_name);

CREATE UNIQUE INDEX IF NOT EXISTS tract_congestion_recorded_at_tract_name_idx
  ON surge_prediction.tract_congestion (recorded_at, tract_name);

CREATE MATERIALIZED VIEW IF NOT EXISTS surge_prediction.speed_tract_vectors
  AS
    SELECT
      st.recorded_at,
      array_agg(c.mean_speed
      ORDER BY c.tract_name) AS mean_speed,
      array_agg(c.var_speed
      ORDER BY c.tract_name) AS var_speed
    FROM surge_prediction.tract_time_raster st
      JOIN surge_prediction.tract_congestion c ON c.recorded_at = st.recorded_at AND c.tract_name = st.tract_name
    GROUP BY st.recorded_at;

CREATE MATERIALIZED VIEW IF NOT EXISTS surge_prediction.event_tract_vectors
  AS
    SELECT
      st.recorded_at,
      array_agg(e.n_events
      ORDER BY e.tract_name) AS n_events
    FROM surge_prediction.tract_time_raster st
      JOIN surge_prediction.tract_events e ON e.recorded_at = st.recorded_at AND e.tract_name = st.tract_name
    GROUP BY st.recorded_at;

CREATE MATERIALIZED VIEW IF NOT EXISTS surge_prediction.closure_tract_vectors
  AS
    SELECT
      st.recorded_at,
      array_agg(r.n_closures
      ORDER BY r.tract_name) AS n_closures
    FROM surge_prediction.tract_time_raster st
      JOIN surge_prediction.tract_rcrs r ON r.recorded_at = st.recorded_at AND r.tract_name = st.tract_name
    GROUP BY st.recorded_at;

CREATE MATERIALIZED VIEW IF NOT EXISTS surge_prediction.interpolated_weather
  AS
    SELECT
      st.recorded_at,
      w.wind_speed,
      w.precipitation,
      w.temperature,
      w.is_precipitating,
      w.condition
    FROM (SELECT DISTINCT recorded_at
          FROM surge_prediction.tract_time_raster) st
      JOIN surge_prediction.weather_features w
        ON w.recorded_at = date_trunc('hour', st.recorded_at);

VACUUM;