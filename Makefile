all: datasets

datasets: data/surge-prediction-features.h5 data/cluster-centers.uberx.pkl data/cluster-centers.lyft.pkl

_shapefiles: geodata.py
	python geodata.py

_etl: _shapefiles etl_data.py
	python etl_data.py

_cluster: cluster.py _etl
	python cluster.py

data/surge-prediction-features.h5: _etl _cluster

data/cluster-centers.uberx.pkl: _cluster

data/cluster-centers.lyft.pkl: _cluster

data/tracts/tracts.shp: _shapefiles

data/places/places.shp: _shapefiles
